@extends('templates/index')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                  <form>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" id="idUser" value="" disabled hidden>
                            <input type="text" class="form-control" id="inputName" placeholder="">
                          </div>
                          <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" id="inputUsername" placeholder="">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="">
                          </div>
                          <div class="form-group">
                            <label for="">Password</label>
                            <input type="text" class="form-control" id="inputPassword" placeholder="">
                          </div>  
                        </div>
                      </div>
                    </div>
                    <div class="card-footer">
                      <button type="reset" class="btn btn-danger reset">Reset</button>
                      <button type="button" class="btn btn-primary float-right" onclick="addUser()">Submit</button>
                    </div>
                  </form>
                </div>
            </div>  

            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Users</h3>
                </div>
                <div class="card-body table-responsive">
                  <table class="table" id="tbUsers">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Opsi</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
            </div>  
          </div>  
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  var tbUsers = $('#tbUsers').DataTable({
      processing: true,
      serverside: true,
      ajax: { 
          type : 'GET',
          url  : '{{ route('users-datatable')}}',
        }, 
      
      columns:[
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'opsi', name: 'opsi'},
        {data: 'fullname', name: 'fullname'},
        {data: 'username', name: 'username'},
        {data: 'email', name: 'email'},
      ]
  });

  function setUser(id, fullname, username, email) {
    $('#idUser').val(id);
    $('#inputName').val(fullname);
    $('#inputUsername').val(username);
    $('#inputEmail').val(email);
  }
  
  function addUser() {
    var idUser = $('#idUser').val();
    var inputName = $('#inputName').val();
    var inputUsername = $('#inputUsername').val();
    var inputEmail = $('#inputEmail').val();
    var inputPassword = $('#inputPassword').val();

    $.ajax({
      type : 'POST',
      url : '{{ route('users-add') }}',
      data : { 
              'idUser' : idUser,
              'inputName' : inputName,
              'inputUsername' : inputUsername,
              'inputEmail' : inputEmail,
              'inputPassword' : inputPassword,
            },
      success: function (e) {
        notif(e.code, e.message);
        if (e.code == 200) {
          clearForm();
          tbUsers.ajax.reload();
        }
      }
    });
  }

  function clearForm() {
    $('#idUser').val('');
    $('#inputName').val('');
    $('#inputUsername').val('');
    $('#inputEmail').val('');
    $('#inputPassword').val('');
  }

  function deleteUser(id) {
    $.ajax({
      type : 'POST',
      url : '{{ route('users-delete') }}',
      data : { 
              'inputIdUser' : id,
            },
      success: function (e) {
        notif(e.code, e.message);
        if (e.code == 200) {
          // clearForm();
          tbUsers.ajax.reload();
        }
      }
    });
  }


</script>
@endpush