@extends('templates/index')

@section('content')
<div class="content">
  <input type="text" id="_id" value="{{ $idPerusahaan }}" disabled hidden>
  <div class="modal fade" id="addNode">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Node Setting</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="fm">
            <div class="form-group">
              <label for="">Chip SN</label>
              {{-- <input type="text" class="form-control" id="inputChip" value=""> --}}
              <input type="text" class="form-control" id="inputIdNodeSetting" value="" disabled hidden>
              <select name="" class="form-control listChip" id="inputChip"></select>
            </div>
            <div class="form-group">
              <label for="">No. Sensor</label>
              <input type="text" class="form-control" id="inputNoSensor" value="">
            </div>
            {{-- <div class="form-group">
              <label for="">Kebun</label>
              <select class="form-control select2" id="inputIdKebun">  
                <option></option>
              </select>
            </div> --}}
            {{-- <div class="form-group">
              <label for="">Tipe</label>
              <input type="text" class="form-control" id="inputIdTipe" value="">
            </div> --}}
            <div class="form-group">
              <label for="">Nama Node</label>
              <input type="text" class="form-control" id="inputSensor" value="">
            </div>
            {{-- <div class="form-group">
              <label for="">Tampil Di Beranda</label>
              <input type="text" class="form-control" id="inputIdOpsi" value="">
            </div> --}}
            <div class="form-group">
              <label for="">Keterangan</label>
              <textarea type="text" rows="2" class="form-control" id="inputKeterangan" value=""></textarea>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="reset" class="btn btn-danger reset-select">Reset</button>
          <button type="button" class="btn btn-primary" onclick="addNode()">Save</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Node Setting</h3>
          <div class="card-tools">
            <button data-toggle="modal" data-target="#addNode" class="btn btn-success modalAddNode reset-select">
              <i class="fas fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="card-body table-responsive">
          <table class="table table-hover text-nowrap" id="tbNode" width="100%">
            <thead>
              <tr>
                <th>Opsi</th>
                <th>ID Node</th>
                <th>CHIP SN</th>
                <th>No Sensor</th>
                <th>Tipe</th>
                <th>Kebun</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>  
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  var _id = $('#_id').val();

  var listChip = $('.listChip');
  listChip.select2({
    theme: 'bootstrap4',
    ajax:{
      type: "POST",
      url : '{{ route('api-chip') }}',
      data: function (e) {
        return {
          '_id' : _id,
        }
      },
      processResults: function (e) {
        return {
          results : $.map(e, function (item) {
            return {
              text : item.chip+" / "+item.keterangan,
              id : item.id 
            }
          })
        }
      }
    }
  });

  $(document).on("click", ".reset-select", function (event) {
    resetSelect();    
    clearFormNode();
  });

  function resetSelect() {
    $("#inputChip").val("")
      .trigger('change')
      .trigger('select2:select');
  }

  function clearFormNode() {
    $('#inputIdNodeSetting').val("");
    $("#inputChip").val("")
      .trigger('change')
      .trigger('select2:select');

    $('#inputNoSensor').val("");
    $('#inputSensor').val("");
    $('textarea#inputKeterangan').text("");
  }

  var tbNode = $('#tbNode').DataTable({
      processing: true,
      serverside: true,
      ajax: { 
              type : 'POST',
              url  : '{{ route('admin-node-datatable')}}',
              data : function (e) {
                e._id = $('#_id').val();
              }
            }, 
      columns:[
                {data: 'opsi', name: 'opsi'},
                {data: 'id', name: 'id'},
                {data: 'chip', name: 'chip'},
                {data: 'sub_node', name: 'sub_node'},
                {data: 'nama_tipe', name: 'nama_tipe'},
                {data: 'nama_kebun', name: 'nama_kebun'},
                {data: 'keterangan', name: 'keterangan'}
              ]
  });

  function setNode(id, namaChip, idChip, subNode, namaNode, ket) {
    $('#addNode').modal("show");

    $('#inputIdNodeSetting').val(id);
    $('#inputChip').val(idChip);

    var opt = new Option(namaChip, idChip, true, true);
    $("#inputChip").append(opt)
      .trigger('change')
      .trigger('select2:select');

    $('#inputNoSensor').val(subNode);
    $('#inputSensor').val(namaNode);
    $('textarea#inputKeterangan').text(ket);
  }

  function deleteNode(id) {
    $.ajax({
      type : 'POST',
      url : '{{ route('node-delete') }}',
      data : { 
              '_id': id,
            },
      success: function (e) {
        notif(e.code, e.message);
        if (e.code == 200) {
          tbNode.ajax.reload();
        }
      }
    });
  }

  function addNode() {
    var inputIdNodeSetting = $('#inputIdNodeSetting').val();
    var inputChip = $('#inputChip').val();
    var inputNoSensor = $('#inputNoSensor').val();
    var inputSensor = $('#inputSensor').val();
    var inputKeterangan = $('textarea#inputKeterangan').val();
    
    $.ajax({
      type : 'POST',
      url : '{{ route('admin-node-add') }}',
      data : { 
              inputIdNodeSetting : inputIdNodeSetting,
              inputChip : inputChip,
              inputNoSensor : inputNoSensor,
              inputSensor : inputSensor,
              inputKeterangan : inputKeterangan
            },
      success: function (e) {
        notif(e.code, e.message);
        if (e.code == 200) {
          clearFormNode();
          $('#addNode').modal('hide');
          tbNode.ajax.reload();
        }
      }
    });
  }
  
      
</script>
@endpush