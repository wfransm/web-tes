@extends('templates/index')

@section('content')
<div class="content">
  <div class="error-page">
    <h2 class="headline text-warning"> 404</h2>

    <div class="error-content">
      <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Halaman tidak tersedia !!!.</h3>

      <p>
        Kembali ke <a href="{{ route('dashboard') }}"> Dashboard</a>
      </p>
    </div>
    <!-- /.error-content -->
  </div>
      <!-- /.error-page -->
</div>
@endsection

@push('js')
<script type="text/javascript">
  
      
</script>
@endpush