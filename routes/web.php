<?php

use Illuminate\Support\Facades\Route;
// Route::group(['middleware' => 'web'], function () {
Route::group(['middleware' => ['web']], function () {
    
    Route::get('/', 'LoginController@index')->name('login');
    Route::post('login-post', 'LoginController@postLogin')->name('login-post');
    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::prefix('dashboard')->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
    });
    
    Route::prefix('users')->group(function () {
        Route::get('/', 'UsersController@index')->name('users');
        Route::get('data', 'UsersController@data')->name('users-datatable');
        Route::post('save', 'UsersController@add')->name('users-add');
        Route::post('delete', 'UsersController@delete')->name('users-delete');
    });
});