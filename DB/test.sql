-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2023 at 09:09 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_level` int(11) NOT NULL DEFAULT 3,
  `id_perusahaan` int(11) NOT NULL DEFAULT 1,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT 'NULL',
  `pwd` varchar(100) DEFAULT NULL,
  `flag_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '0:blocked 1:active ',
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updater` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_level`, `id_perusahaan`, `username`, `email`, `fullname`, `pwd`, `flag_active`, `tgl_update`, `updater`) VALUES
(1, 1, 1, 'admin', 'wfransm@gmail.com', 'Admin System', '$2y$10$nsJk6zaNjeyqmC7GrHLs1OaxORLJIqDZ5q/RFIbcj0jxqAEJNUCCC', 1, '2023-06-10 07:59:44', '1'),
(2, 6, 2, 'budi', 'budi@gmail.com', 'Budi Admin PT A', '$2y$10$lMEum/vsA6fDv.GxqgJQE.M/T5UBObDUgsocS59il8QzUxgQxW5MG', 1, '2023-06-08 11:18:00', NULL),
(3, 7, 3, 'eko', 'eko@gmail.com', 'Eko Admin PT B', '$2y$10$aHQkWLk1On1O8Q7hVMkxD.JB2/vY08HDxfjZJrspoebUutEcXpYk2', 1, '2023-06-08 11:18:05', NULL),
(4, 2, 3, 'ari', 'ari@gmail.com', 'Ari', '$2y$10$aUuMkHMlRScfaRKiuRy5weE4tXlzdu19xScqDMG1RARo55bMMG4lq', 9, '2023-06-08 04:05:51', NULL),
(6, 1, 1, 'admin2', 'adm@gmail.com', 'Admin System', '$2y$10$nsJk6zaNjeyqmC7GrHLs1OaxORLJIqDZ5q/RFIbcj0jxqAEJNUCCC', 9, '2023-08-16 18:44:18', '1'),
(7, 3, 2, 'A', 'a@gmail.com', 'A', '$2y$10$WeUwyZEqoS4JoMs31dbB2uhCJaI.rBuTanArADQ8W9KnB9STAzfOu', 9, '2023-08-16 18:44:20', NULL),
(8, 5, 2, 'be', 'b@gmail.com', 'B', '$2y$10$NEQLFAB0.9UC8FojlHeuTugNU4HhuB6X14QHTtIQ2zx7a5/KftXFu', 9, '2023-08-16 18:44:22', NULL),
(9, 4, 2, 'c', 'c@gmail.com', 'C', '$2y$10$SOv/EnFVlwXw5NAabJWK4e5.1WQNlEA5PUng8dkdoBnrX3Yy2Sh9O', 9, '2023-08-16 18:44:24', NULL),
(10, 1, 1, 'aaaaa', 'ssasasa@gmail.com', 'aasdasdsa', '$2y$10$ab02vsU29fa4A3sSaT7lqeJiuioQKOFl6.dmob99wEMPAdtphl6Ti', 9, '2023-08-16 18:57:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_level`
--

CREATE TABLE `users_level` (
  `id` int(5) NOT NULL,
  `id_perusahaan` int(5) DEFAULT NULL COMMENT 'id = 0 default untuk semua perusahaan',
  `nama` varchar(50) NOT NULL,
  `last_chg_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_chg_user` int(5) NOT NULL COMMENT 'user terakhir pengubah record',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'status aktif 1 atau dimatikan 0(hapus)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_level`
--

INSERT INTO `users_level` (`id`, `id_perusahaan`, `nama`, `last_chg_time`, `last_chg_user`, `is_active`) VALUES
(1, 0, 'Admin System', '2020-09-25 19:09:36', 1, 1),
(2, 0, 'Admin Perusahaan', '2020-09-26 01:42:00', 1, 1),
(3, 0, 'Operator Kebun', '2020-09-25 19:12:34', 1, 1),
(4, 0, 'Pengamat Kebun', '2020-09-25 19:13:34', 1, 1),
(5, 0, 'Umum', '2020-09-25 19:14:34', 1, 1),
(6, 1, 'A', '2023-06-08 11:07:30', 1, 0),
(7, 2, 'B', '2023-06-08 11:08:46', 1, 0),
(8, 2, 'setting', '2023-06-09 12:33:25', 1, 0),
(9, 0, 'tes', '2023-07-13 09:47:48', 1, 0),
(10, 0, 'tes', '2023-07-13 09:47:48', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_level`
--
ALTER TABLE `users_level`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users_level`
--
ALTER TABLE `users_level`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
