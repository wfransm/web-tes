<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use DB;
use Hash;
use Session;
// use Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function postLogin(Request $req)
    {
        $account = $req->inputEmail;
        $password = $req->inputPassword;

        $field = filter_var($account, FILTER_VALIDATE_EMAIL) ? 'a.email' : 'a.username';

        $users = DB::table('users as a')
                    ->where($field, $account)
                    ->where('a.flag_active', 1)
                    ->leftJoin('users_level as b', 'b.id', '=', 'a.id_level')
                    // ->leftJoin('perusahaan as c', 'c.id', '=', 'a.id_perusahaan')
                    ->select('a.id', 'a.id_level', 'a.id_perusahaan', 'a.username', 'a.email', 'a.fullname', 'a.pwd', 'a.flag_active',
                                'b.nama as user_level',
                                    // 'c.nama as nama_perusahaan'
                                    )
                    ->first();


        if ( $users && Hash::check($password, $users->pwd)) {

            Session::put('email',$users->email);
            Session::put('username',$users->username);
            Session::put('password',$password);
            Session::put('fullname',$users->fullname);
            Session::put('idLevel',$users->id_level);
            Session::put('userLevel',$users->user_level);
            Session::put('idPerusahaan',$users->id_perusahaan);
            // Session::put('namaPerusahaan',$users->nama_perusahaan);

            return redirect()->route('dashboard');
        }else {
            Session::flush();

            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Session::flush();
        // Auth::logout();
        return redirect()->route('login');
    }
}
