<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use DB;
use Hash;

class UsersController extends Controller
{
    public function index()
    {
        $usersLevel = DB::table('users_level')->get();

        $data = [
            'users_level' => $usersLevel
        ];

        return view('users.index')->with($data);
    }

    public function data()
    {
       $users = DB::table('users as a')
                    ->leftJoin('users_level as b', 'b.id', '=', 'a.id_level')
                    ->where('a.flag_active', '!=', 9)
                    ->select('a.id', 'a.id_level', 'a.id_perusahaan', 'a.username', 'a.email', 'a.fullname', 'a.flag_active as status',
                                'b.nama as akses')
                    ->get();

       return Datatables::of($users)
        ->addIndexColumn()
        ->addColumn('opsi', function ($users) {
            $idEncode = "'".base64_encode($users->id)."'";

            $fullname = "'".$users->fullname."'";
            $username = "'".$users->username."'";
            $email = "'".$users->email."'";
            
            $buttonEdit = '<button class="btn btn-sm btn-primary btn-flat" onclick="setUser('.$idEncode.', '.$fullname.', '.$username.', '.$email.')" ><i class="fas fa-edit"></i></button>';
            $buttonDelete = '<button type="button" class="btn btn-sm btn-danger btn-flat" onclick="deleteUser('.$idEncode.')"><i class="fas fa-trash"></i></button>';
            return $buttonEdit.'&nbsp'.$buttonDelete;
        })
        ->editColumn('status', function ($users) {
            return ($users->status == 1) ? 'aktif' : 'tidak aktif';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function add(Request $req)
    {
        $idUser = $req->idUser;
        $inputName = $req->inputName;
        $inputUsername = $req->inputUsername;
        $inputEmail = $req->inputEmail;
        $inputPassword = $req->inputPassword;

        if ( $inputName && $inputEmail && $inputPassword ) {
            $inputData = [
                            'id_level' => 1,
                            'id_perusahaan' => 1,
                            'username' => $inputUsername,
                            'email' => $inputEmail,
                            'fullname' => $inputName,
                            'pwd' => Hash::make($inputPassword),
                            'flag_active' => 1
                        ];

            DB::beginTransaction();
            try {
                if ( is_null($idUser) ) {
                    $save = DB::table('users')->insert($inputData);
                }else {
                    $save = DB::table('users')->where('id', '=', base64_decode($idUser))->update([
                                                                                                    'username' => $inputUsername,
                                                                                                    'email' => $inputEmail,
                                                                                                    'fullname' => $inputName,
                                                                                                    'pwd' => Hash::make($inputPassword),
                                                                                                ]);                    
                }

                DB::commit();
                $response = [
                    'code' => 200,
                    'message' => 'Berhasil ditambahkan'
                ];
            } catch (Exception $e) {
                DB::rollback();
                $response = [
                    'code' => 400,
                    'message' => 'Gagal ditambahkan !'
                ];
            }
        }else {
            $response = [
                'code' => 300,
                'message' => 'Harap isi Nama Perusahaan, Dirut, & Alamat !'
            ];
        }

        return response()->json($response);
    }

    public function delete(Request $req)
    {
        $idUserEncode = base64_decode($req->inputIdUser);
        $delete = DB::table('users')
                    ->where('id', $idUserEncode)
                    ->update(['flag_active' => 9]);
        
        $response = [];

        if ($delete) {
            $response = [
                    'code' => 200,
                    'message' => 'Berhasil dihapus'
                ];
        }else {
            $response = [
                    'code' => 200,
                    'message' => 'Gagal dihapus !'
                ];
        }

        return response()->json($response);
    }

}
